# veggiecom-api
Project for new Veggie Community Backend

## Getting started:
* first, start the database:
```bash
$ docker-compose up
```
* then start the application via the Maven wrapper:
```bash
# on Linux and macOS
$ ./mvnw spring-boot:run

# on Windows cmd
> mvnw.cmd spring-boot:run
```

* open localhost:8080 or localhost:8080/hello/{yourName}

If you want to stop the service press `Ctrl`+`C`.

## Run project locally via Docker

### Start the whole project

To run the backend locally without any additional tools you can use Docker. Build the latest package and run the service
afterwards:

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.api.yml up
```

You may need to use `sudo` on Linux.

This will start the database, build and run the api and expose it on port 8080. When you see a log output similar to:

> api_1  | 2020-05-24 09:36:47.706  INFO 1 --- [           main] DeferredRepositoryInitializationListener : Spring Data repositories initialized!
> api_1  | 2020-05-24 09:36:47.731  INFO 1 --- [           main] d.v.v.VeggiecomApiApplicationKt          : Started VeggiecomApiApplicationKt in 7.839 seconds (JVM running for 8.797)

then you can open the api in the browser:

* http://localhost:8080/hello
* http://localhost:8080/hello/foo
* http://localhost:8080/actuator/health

#### Stop the service

To stop the service press `Ctrl`+`C` in the terminal where it is running.

#### Delete the service

If you want to delete the containers make sure to stop the service and run:

```bash
docker-compose -f docker-compose.yml -f docker-compose.api.yml down
```

### Start over

Sometimes you may need to do re-build of the application. To do so, you can
simply add `--build` at the end like so:

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.api.yml up --build
```

**This will not delete the database though.**
