FROM openjdk:8-jdk-alpine AS build
WORKDIR /workdir
COPY .mvn .mvn
COPY mvnw mvnw
COPY pom.xml .
COPY src src
RUN /bin/sh ./mvnw package -DskipTests

FROM openjdk:8-jre-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY --from=build /workdir/target/*.jar app.jar
HEALTHCHECK --start-period=15s --interval=1m --timeout=10s --retries=5 \
            CMD curl --silent --fail --request GET http://localhost:8080/actuator/health \
                | jq --exit-status '.status == "UP"' || exit 1
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "/app.jar" ]
