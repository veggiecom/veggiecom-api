package de.veggiecom.veggiecombackend.application.response

data class UserInfoResponse(val name: String)