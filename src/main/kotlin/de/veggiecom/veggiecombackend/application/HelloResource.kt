package de.veggiecom.veggiecombackend.application

import de.veggiecom.veggiecombackend.application.response.UserInfoResponse
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.keycloak.representations.AccessToken
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.PathVariable


@CrossOrigin(origins = ["*"])
@RestController
class HelloResource {

    @GetMapping("hello")
    fun sayHello(authentication: KeycloakAuthenticationToken): UserInfoResponse {
        val accessToken: AccessToken = authentication.getAccount().getKeycloakSecurityContext().getToken()
        val userInfoResponse = UserInfoResponse(accessToken.givenName);
        return userInfoResponse;
    }

    @RequestMapping("hello/{name}")
    fun hello(@PathVariable name: String): String {
        return "Hello " + name + "!"
    }
}
