package de.veggiecom.veggiecombackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VeggiecomApiApplication

fun main(args: Array<String>) {
    runApplication<VeggiecomApiApplication>(*args)
}
