package de.veggiecom.veggiecombackend

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody

@ActiveProfiles("integration-test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HelloResourceIT {

    @Test
    fun shouldUnauthorizeGreeting(@Autowired webClient: WebTestClient) {

        webClient.get()
                .uri("/hello")
                .accept(MediaType.TEXT_PLAIN)
                .exchange()
                .expectStatus()
                .isUnauthorized
    }


    @Test
    fun shouldReturnGreeting(@Autowired webClient: WebTestClient) {

        webClient.get()
                .uri("/hello/Foo")
                .accept(MediaType.TEXT_PLAIN)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody<String>()
                .isEqualTo("Hello Foo!")
    }
}
